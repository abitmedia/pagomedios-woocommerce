<?php
/**
 * Plugin Name: Pagomedios - botón de pagos
 * Plugin URI: https://www.pagomedios.com
 * Description: Integre cobros con tarjetas de crédito y débito: <strong>Visa, MasterCard, Diners, Discover y American Express</strong> en corriente y diferido. Procesamiento con <strong>Datafast, Alignet, Paymentez o Payphone</strong>. Pagomedios es parte de la suite empresarial de aplicaciones en la nube Abitmedia Cloud.
 * Author: Abitmedia Cloud
 * Author URI: https://www.abitmedia.com
 * Version: 1.0
 * Text Domain: wp-wc-pagomedios-datafast
 * Domain Path: /i18n/languages/
 *
 * Copyright: (c) 2016-2019 Abitmedia Cloud Pagomedios. (info@abitmedia.com) and WooCommerce
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package   wp-wc-pagomedios-datafast
 * @author    Abitmedia
 * @category  Admin
 * @copyright Copyright (c) 2016-2020, Abitmedia
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 *
 * This offline gateway forks the WooCommerce core "Cheque" payment gateway to create another offline payment method.
 */

defined( 'ABSPATH' ) or exit;

// Make sure WooCommerce is active
if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	return;
}


/**
 * Add the gateway to WC Available Gateways
 * 
 * @since 1.0.0
 * @param array $gateways all available WC gateways
 * @return array $gateways all WC gateways + offline gateway
 */
function wc_pagomedios_datafast_add_to_gateways( $gateways ) {
	$gateways[] = 'WC_Gateway_Pagomedios_Datafast';
	return $gateways;
}
add_filter( 'woocommerce_payment_gateways', 'wc_pagomedios_datafast_add_to_gateways' );


/**
 * Adds plugin page links
 * 
 * @since 1.0.0
 * @param array $links all plugin links
 * @return array $links all plugin links + our custom links (i.e., "Settings")
 */
function wc_pagomedios_datafast_gateway_plugin_links( $links ) {

	$plugin_links = array(
		'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=pagomedios' ) . '">' . __( 'Configurar', 'wc-pagomedios-datafast' ) . '</a>',
		'<a target="_blank" href="https://docs.cloud.abitmedia.com/category/pagomedios-cobros-con-tarjetas">' . __( 'Documentación', 'wc-pagomedios-datafast' ) . '</a>',
	);

	return array_merge( $plugin_links, $links );
}
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'wc_pagomedios_datafast_gateway_plugin_links' );


/**
 * Offline Payment Gateway
 *
 * Provides an Offline Payment Gateway; mainly for testing purposes.
 * We load it later to ensure WC is loaded first since we're extending it.
 *
 * @class 		WC_Gateway_Pagomedios_Datafast
 * @extends		WC_Payment_Gateway
 * @version		1.0.0
 * @package		WooCommerce/Classes/Payment
 * @author 		SkyVerge
 */
add_action( 'plugins_loaded', 'wc_pagomedios_datafast_gateway_init', 11 );

function wc_pagomedios_datafast_gateway_init() {

	class WC_Gateway_Pagomedios_Datafast extends WC_Payment_Gateway {

		/**
		 * Constructor for the gateway.
		 */


		public function __construct() {
			
			$this->options = [
				''	=>	'Seleccione',
				'1' =>	'Alignet Payme',
				'2' =>	'Payphone',
				'3' =>	'Datafast',
				'4' =>	'Paymentez',
			];

			$this->imagen_procesadora = '';
			$this->imagen_tarjetas = '';

			$this->id                 = 'pagomedios';
			$this->icon               = ( $this->get_option( 'pasarela' ) == 1 || $this->get_option( 'pasarela' ) == 2 ) ? plugin_dir_url( __FILE__ ).'images/tarjetas-visa-mastercard.jpg' : plugin_dir_url( __FILE__ ).'images/tarjetas-full.jpg';
			// $this->has_fields         = false;
			$this->method_title       = __( 'Pagomedios', 'wc-pagomedios-datafast' );
			$this->method_description = __( 'Configure su tienda en línea WooCommerce para recibir pagos en línea con tarjetas Visa, MasterCard, Diners, Discover y American Express.', 'wc-pagomedios-datafast' );
		  
			// Load the settings.
			$this->init_form_fields();
			$this->init_settings();
		  
			// Define user set variables
			$this->title        = 'Tarjeta de crédito/débito';

			if( $this->get_option( 'pasarela' ) == 1 ){
				$this->imagen_procesadora = 'payme.jpg';
				$this->imagen_tarjetas = 'tarjetas-visa-mastercard.jpg';
			}elseif( $this->get_option( 'pasarela' ) == 2 ){
				$this->imagen_procesadora = 'payphone.jpg';
				$this->imagen_tarjetas = 'tarjetas-visa-mastercard.jpg';
			}elseif( $this->get_option( 'pasarela' ) == 3 ){
				$this->imagen_procesadora = 'datafast.jpg';
				$this->imagen_tarjetas = 'tarjetas-full.jpg';
			}elseif( $this->get_option( 'pasarela' ) == 4 ){
				$this->imagen_procesadora = 'paymentez.jpg';
				$this->imagen_tarjetas = 'tarjetas-full.jpg';
			}

			$this->description  = 'Pago seguro con <a target="_blank" href="https://pagomedios.com/">Pagomedios</a> - '.$this->options[$this->get_option( 'pasarela' )].' <br><img width="280px" src="'.plugin_dir_url( __FILE__ ).'images/'.$this->imagen_procesadora.'"> <img width="280px" src="'.plugin_dir_url( __FILE__ ).'images/'.$this->imagen_tarjetas.'"><small><br>Será redireccionado a un ambiente seguro de pago</small>';

			$this->instructions = 'Instrucciones';
		  
			// Actions
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page' ) );
		  
			// Customer Emails
			add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
		}
	
	
		/**
		 * Initialize Gateway Settings Form Fields
		 */
		public function init_form_fields() {

			$this->form_fields = apply_filters( 'wc_offline_form_fields', array(
		  

				'enabled' => array(
					'title'   => __( 'Activar/Desactivar', 'wc-pagomedios-datafast' ),
					'type'    => 'checkbox',
					'label'   => __( 'Habilitar o deshabilitar método de pago', 'wc-pagomedios-datafast' ),
					'default' => 'yes'
				),

				'token' => array(
		        	'title' => __( 'Token de comercio', 'wc-gateway-offline' ),
		          	'type' => 'textarea',
		          	'description' => __( 'Encuentre su token en: Abitmedia Cloud -> Panel de usuario -> Mi empresa -> Token', 'wc-gateway-offline' ),
		          	// 'desc_tip'    => true,
		        ),

				'pasarela' => array(
					'title'             => __( 'Pasarela de pagos', 'wc-pagomedios-datafast' ),
					'type'              => 'select',
					'class'             => 'wc-enhanced-select',
					'css'               => 'width: 400px;',
					'default'           => '',
					'description'       => __( 'Asegurese de seleccionar la pasarela que tenga configurada en Pagomedios, caso contrario se procesará con la que tiene contratada. ¿No tiene configurada ningún pasarela? <a href="https://abitmedia.com/contactos/" target="_blank">Contáctese con soporte</a>' ),
					'options'           => $this->options,
					// 'desc_tip'          => true,
					'custom_attributes' => array(
						'data-placeholder' => __( 'Seleccione', 'wc-pagomedios-datafast' ),
					),
				),

				'facturacion' => array(
					'title'   => __( 'Facturación electrónica', 'wc-pagomedios-datafast' ),
					'type'    => 'checkbox',
					'label'   => __( 'Emitir factura electrónica después de autorizarse el cobro', 'wc-pagomedios-datafast' ),
					'default' => 'no',
					'description' => __( '¿No tiene contratado este módulo? <a href="https://abitmedia.com/contactos/" target="_blank">Contáctese con ventas</a>', 'wc-gateway-offline' ),
				),

			) );
		}
	
	
		/**
		 * Output for the order received page.
		 */
		public function thankyou_page() {
			if ( $this->instructions ) {
				echo wpautop( wptexturize( $this->instructions ) );
			}
		}
	
	
		/**
		 * Add content to the WC emails.
		 *
		 * @access public
		 * @param WC_Order $order
		 * @param bool $sent_to_admin
		 * @param bool $plain_text
		 */
		public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
		
			// if ( $this->instructions && ! $sent_to_admin && $this->id === $order->payment_method && $order->has_status( 'on-hold' ) ) {
			// 	echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
			// }
		}
	
	
		/**
		 * Process the payment and return the result
		 *
		 * @param int $order_id
		 * @return array
		 */
		public function process_payment( $order_id ) {
			$cart = WC()->cart;
			$amount = $cart->get_total(false);
			$amountWithTax = 0;
			$amountWithoutTax = 0;
			$tax = $cart->get_total_tax();

			foreach ( $cart->get_cart() as $cart_item ) {
				$product = wc_get_product( $cart_item['product_id'] );
				$taxClass = new WC_Tax();
				$validate_tax = $taxClass->get_rates($product->get_tax_class());
				if( count($validate_tax) > 0 ){
					$amountWithTax += $cart_item['data']->get_price('edit') * $cart_item['quantity'];
				}else{
					$amountWithoutTax += $cart_item['data']->get_price('edit') * $cart_item['quantity'];
				}
			}
			if( $cart->get_shipping_tax() > 0 ){
				$amountWithTax += $cart->get_shipping_total();
			}else{
				$amountWithoutTax += $cart->get_shipping_total();
			}

			if( $cart->get_discount_tax() > 0 ){
				$amountWithTax -= $cart->get_discount_total();
			}else{
				$amountWithoutTax -= $cart->get_discount_total();
			}

			if( $cart->get_fee_tax() > 0 ){
				$amountWithTax += $cart->get_fee_total();
			}else{
				$amountWithoutTax += $cart->get_fee_total();
			}

			$order = new WC_Order( $order_id );
			
			$url = 'https://cloud.abitmedia.com/api/payments/create-payment-request';
			// $url = 'http://localhost/abitmedia-cloud/web/api/payments/create-payment-request';

			$headers = array(
			    "Content-Type: application/x-www-form-urlencoded",
			    "Postman-Token: 3724770d-a4c7-4330-97dc-6d66237dbc19",
			    "cache-control: no-cache",
			    sprintf('Authorization: Bearer %s', $this->get_option( 'token' ))
			);

			$data = [
			    'companyType' => $_POST['billing_company_type'], //Persona Natural, Empresa
			    'document' => $_POST['billing_document'], 
			    'documentType' => $_POST['billing_document_type'], //01 Cédula de identidad, 02 RUC, Pasaporte, 03 ID del exterior
			    'fullName' => $order->billing_first_name.' '.$order->billing_last_name,
			    'address' => $order->billing_address_1,
			    'mobile' => $order->billing_phone,
			    'email' => $order->billing_email,
			    'description' => 'Pago desde '.get_bloginfo('name'),
			    'amount' => (float)$amount,
			    'amountWithTax' => (float)$amountWithTax,
			    'amountWithoutTax' => (float)$amountWithoutTax,
			    'tax' => (float)$tax,
			    'notifyUrl' => plugin_dir_url( __FILE__ ).'response_pagomedios.php',
			    'reference' => $order_id,
			    'gateway' => $this->get_option( 'pasarela' ),
			    'generateInvoice' => ( $this->get_option( 'facturacion' ) == 'yes' ) ? 1 : 0,
			];

			$curl = curl_init();
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_TIMEOUT, 30);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			  	wc_add_notice( $err, 'error' );
				return;
			} else {
			  	$response = json_decode($response);
			  	if( $response->code == 1 ){
			    	return array(
						'result' 	=> 'success',
						'redirect'	=> $response->data->url,
					);
			    }else{
			    	$display = '<ul>';
			    	$errors = $response->errors;
					foreach ($errors as $attribute => $messages) {
						$display.= '<li>'.implode(',', $messages).'</li>';
					}
					$display .= '</ul>';
			    	wc_add_notice( $display, 'error' );
					return;
			    }

			}

		}
	
  }
}

// Tipo de empresa
add_action( 'woocommerce_before_checkout_billing_form', 'custom_checkout_field_billing_companyType' );
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta1' );

if( !function_exists( 'custom_checkout_field_billing_companyType' ) ){
	function custom_checkout_field_billing_companyType( $checkout ) {

	    woocommerce_form_field( 'billing_company_type', array(
	        'type'          => 'select',
	        'label'         => __('Tipo de empresa'),
	        'placeholder'   => __(''),
	        'required' 		=> true,
	        'options'       => array(
		    	''		=> __( 'Seleccione', 'wps' ),
		        'Persona Natural'	=> __( 'Persona Natural', 'wps' ),
		        'Empresa'	=> __( 'Empresa', 'wps' )
		    )
	        ), $checkout->get_value( 'billing_company_type' ));

	}
}

if( !function_exists( 'my_custom_checkout_field_update_order_meta1' ) ){
	function my_custom_checkout_field_update_order_meta1( $order_id ) {
	    if ( ! empty( $_POST['billing_company_type'] ) ) {
	        update_post_meta( $order_id, 'billing_company_type', sanitize_text_field( $_POST['billing_company_type'] ) );
	    }
	}
}
// Tipo de empresa



//Tipo de documento
add_action( 'woocommerce_before_checkout_billing_form', 'custom_checkout_field_billing_documentType' );
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta2' );
if( !function_exists( 'custom_checkout_field_billing_documentType' ) ){
	function custom_checkout_field_billing_documentType( $checkout ) {

	    woocommerce_form_field( 'billing_document_type', array(
	        'type'          => 'select',
	        'label'         => __('Tipo de documento'),
	        'placeholder'   => __(''),
	        'required' 		=> true,
	        'options'       => array(  //01 Cédula de identidad, 02 RUC, 03 ID del exterior
		    	''		=> __( 'Seleccione', 'wps' ),
		        '01'	=> __( 'Cédula de identidad', 'wps' ),
		        '02'	=> __( 'RUC', 'wps' ),
		        '03'	=> __( 'Pasaporte', 'wps' ),
		        '04'	=> __( 'ID del exterior', 'wps' ),
		    )
	        ), $checkout->get_value( 'billing_document_type' ));

	}
}

if( !function_exists( 'my_custom_checkout_field_update_order_meta2' ) ){
	function my_custom_checkout_field_update_order_meta2( $order_id ) {
	    if ( ! empty( $_POST['billing_document_type'] ) ) {
	        update_post_meta( $order_id, 'billing_document_type', sanitize_text_field( $_POST['billing_document_type'] ) );
	    }
	}
}
//Tipo de documento



//Identificación
add_action( 'woocommerce_before_checkout_billing_form', 'custom_checkout_field_billing_document' );
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta3' );

if( !function_exists( 'custom_checkout_field_billing_document' ) ){
	function custom_checkout_field_billing_document( $checkout ) {

	    woocommerce_form_field( 'billing_document', array(
	        'type'          => 'text',
	        'label'         => __('Identificación'),
	        'placeholder'   => __(''),
	        'required' 		=> true,
	        ), $checkout->get_value( 'billing_document' ));

	}
}

if( !function_exists( 'my_custom_checkout_field_update_order_meta3' ) ){
	function my_custom_checkout_field_update_order_meta3( $order_id ) {
	    if ( ! empty( $_POST['billing_document'] ) ) {
	        update_post_meta( $order_id, 'billing_document', sanitize_text_field( $_POST['billing_document'] ) );
	    }
	}
}
//Identificación

add_action( 'woocommerce_before_checkout_form', 'wpdesk_checkout_hello', 5 );
/**
 * Add "hello" text before WooCommerce checkout form
 *
 */
function wpdesk_checkout_hello() {
 //  	$cart = WC()->cart;

	// $amount = $cart->get_total(false);
	// $amountWithTax = 0;
	// $amountWithoutTax = 0;
	// $tax = $cart->get_total_tax();

	// foreach ( $cart->get_cart() as $cart_item ) {
	// 	$product = wc_get_product( $cart_item['product_id'] );
	// 	$taxClass = new WC_Tax();
	// 	$validate_tax = $taxClass->get_rates($product->get_tax_class());
	// 	if( count($validate_tax) > 0 ){
	// 		$amountWithTax += $cart_item['data']->get_price('edit') * $cart_item['quantity'];
	// 	}else{
	// 		$amountWithoutTax += $cart_item['data']->get_price('edit') * $cart_item['quantity'];
	// 	}
	// }
	// if( $cart->get_shipping_tax() > 0 ){
	// 	$amountWithTax += $cart->get_shipping_total();
	// }else{
	// 	$amountWithoutTax += $cart->get_shipping_total();
	// }

	// if( $cart->get_discount_tax() > 0 ){
	// 	$amountWithTax -= $cart->get_discount_total();
	// }else{
	// 	$amountWithoutTax -= $cart->get_discount_total();
	// }

	// if( $cart->get_fee_tax() > 0 ){
	// 	$amountWithTax += $cart->get_fee_total();
	// }else{
	// 	$amountWithoutTax += $cart->get_fee_total();
	// }

	// echo 'amount: '.$amount.'<br>';
	// echo 'amountWithTax: '.$amountWithTax.'<br>';
	// echo 'amountWithoutTax: '.$amountWithoutTax.'<br>';
	// echo 'tax: '.$tax.'<br>';

	// echo '<pre>';
	// print_r( $cart->get_totals() );
	// echo '</pre>';



  // echo 'Subtotal: '.$cart->get_subtotal().'<br>'; //Subtotal del producto nada mas
  // echo 'Subtotal tax: '.$cart->get_total_ex_tax().'<br>'; //aqui no se suma lo del envio *******
  //  echo 'Subtotal tax: '.$cart->get_cart_subtotal(true).'<br>'; //aqui no se suma lo del envio


 //  	foreach( $cart->get_cart() as $cart_item ){
	// 	$product = wc_get_product( $cart_item['product_id'] );
	// 	$tax = new WC_Tax();
	// 	$validacion = $tax->get_rates($product->get_tax_class());
	// 	if( count($validacion) > 0 ){
	// 		echo 'tiene impuesto<br>';
	// 	}else{
	// 		echo 'no tiene impuesto<br>';
	// 	}
	// 	// $cart_items[] = [
	//  //    	'sku' => $product->get_sku(),
	//  //    	'quantity' => $cart_item['quantity'],
	//  //    	'subtotal' => $cart_item['line_subtotal'],
	//  //    ];	  
	// }



  // echo ''.$cart->get_total_ex_tax().'<br>';//se suma productos y envio sin tax
  // echo $cart->get_subtotal().'<br>'; //Subtotal del producto nada mas
  // echo $cart->get_subtotal_tax().'<br>'; //aqui no se suma lo del envio
  // $amount = $order->total;
  // $amountWithTax = $order->;
  // $amountWithoutTax = $order->;
  // $tax = $order->total_tax;

  // echo '<pre>';
  // print_r( $order );
  // echo '</pre>';

}