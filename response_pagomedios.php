<?php 
	require('../../../wp-load.php');
	
	add_action( 'plugins_loaded', 'wc_pagomedios_datafast_gateway_init', 11 );
	add_filter('show_admin_bar', '__return_false');

	$order = wc_get_order( $_POST['reference'] );

	if( $_POST['status'] == 'Pagado' ){
		$order->payment_complete();
		$order->reduce_order_stock();
		WC()->cart->empty_cart();
		wp_redirect( $order->get_checkout_order_received_url() );
	}else{
		wp_redirect( $order->get_checkout_payment_url( $on_checkout = false ) );
	}
?>